import pytest

from homework4_selenium.test_selenium.main_page import MainPage


class TestAddMember:
    @pytest.mark.parametrize("name",["胡次刚"])
    def test_add_member(self,name):
        #用来测试添加成员功能

        self.main = MainPage()
        res1 = self.main.goto_contact().get_list()
        assert name not in res1
        self.main.goto_contact().goto_add_member().add_member()
        res1=self.main.goto_contact().get_list()
        assert name in res1

    # def test_add_member_fail(self):
    #     self.main.goto_add_member().add_member_fail()
