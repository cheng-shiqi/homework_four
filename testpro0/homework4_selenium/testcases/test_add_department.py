#import datas
import pytest
import yaml

from homework4_selenium.test_selenium.contact_page import ContactPage
from homework4_selenium.test_selenium.main_page import MainPage
from homework4_selenium.testcases.conftest import chrome_option


def get_datas():
    with open("/Users/home/PycharmProjects/testpro0/homework4_selenium/testcases/testdata.yaml") as f:
        datas = yaml.safe_load(f)
    return datas


class TestAddDepartment:

    @pytest.mark.parametrize("name", get_datas()["department"])
    def test_data(self, name):
        print(name[0])

    #添加部门
    @pytest.mark.parametrize("name", get_datas()["department"])
    def test_add_department(self, name):
        #self.contact = ContactPage(chrome_option)
        self.main = MainPage()
        # 1.跳转到通讯录页面 2.获取部门列表，做断言验证
        results = self.main.goto_contact().check_add_department(name)
        assert name == results