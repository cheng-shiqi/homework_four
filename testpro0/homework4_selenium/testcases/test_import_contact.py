import datas
import pytest
import yaml
import self as self

from homework4_selenium.test_selenium.contact_page import ContactPage
from homework4_selenium.test_selenium.main_page import MainPage


def get_datas():
    with open("/Users/home/PycharmProjects/testpro0/homework4_selenium/testcases/testdata.yaml") as f:
        datas = yaml.safe_load(f)
    return datas



class TestImportContact:
    # 首页跳转导入通讯录页面
    @pytest.mark.parametrize('contact_file', get_datas()['import_contact_file'])
    def test_import_contact(self, contact_file):
        self.main = MainPage()
        results = self.main.goto_import_contact().import_contact(datas)
        assert contact_file == results

    # 通讯录页面跳转导入通讯录
    @pytest.mark.parametrize('contact_file', get_datas()['import_contact_file'])
    def test_import_contact(self, contact_file):
        print(contact_file)
        self.main = MainPage()
        self.main.goto_contact().goto_import_contact().import_contact(contact_file)
        list = self.main.goto_contact().check_add_memeber()
        print(list)
