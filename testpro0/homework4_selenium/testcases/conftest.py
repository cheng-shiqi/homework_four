#企业微信地址
import pytest
from selenium import webdriver

#Chrome浏览器,普通加载模式
@pytest.fixture(scope="class")
def chrome_load():
    print("开始执行")
    driver = webdriver.Chrome()
    driver.maximize_window()
    driver.implicitly_wait(5) #隐式等待
    yield driver
    print("执行结束")

#Chrome浏览器复用模式
@pytest.fixture(scope="class")
def chrome_option():
    print("开始执行")
    option = webdriver.ChromeOptions()
    option.add_experimental_option('w3c',False)
    option.debugger_address = '127.0.0.1:9222'
    driver = webdriver.Chrome(options=option)
    driver.implicitly_wait(5) #隐式等待
    yield driver
    print("执行结束")

#定义一个查找元素的方法，用于解元祖
def find_element(driver, locator):
    return driver.find_element(*locator)

def find_elements(driver,locator):
    return driver.find_elements(*locator)

#菜单栏，判断菜单项为选中状态
def menu_selected(element):
    '''
    通过element的get_attribute,获取当前菜单项class属性的值，
    当class=frame_nav_item frame_nav_item_Curr，则表示该元素为选中状态
    '''
    if element.get_attribute('class') == 'frame_nav_item frame_nav_item_Curr':
        return True
    else:
        return False
