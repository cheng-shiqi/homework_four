from time import sleep
from selenium.webdriver.common.by import By


from homework4_selenium.test_selenium.add_member_page import AddMemberPage
from homework4_selenium.test_selenium.base_page import BasePage
from homework4_selenium.test_selenium.contact_page import ContactPage
from homework4_selenium.test_selenium.import_contact import ImportContact


class MainPage(BasePage):
    def __init__(self):
        BasePage.__init__(self)
    #跳转到添加成员页面
    def goto_add_member(self):
        pass
        # chrome_arg = webdriver.ChromeOptions()
        # chrome_arg.debugger_address = '127.0.0.1:9222'
        # self.driver=webdriver.Chrome(options=chrome_arg)
        #add_member_ele=By.CSS_SELECTOR, ".ww_indexImg_AddMember"
        #self.driver.find_elements(By.CSS_SELECTOR, ".ww_indexImg_AddMember").click()
        #self.find(add_member_ele).click()
        #add_member_ele=By.CSS_SELECTOR, "js_has_member ww_operationBar js_add_member"
        #self.find(add_member_ele).click()
        # self.goto_contact()
        # #self.driver.find_element(By.CSS_SELECTOR, ".qui_btn.ww_btn.js_add_member").click()
        # # self.driver.refresh()
        # # ele = self.driver.find_element_by_css_selector('a.qui_btn.ww_btn.js_add_member')
        # # print(ele.text)
        # # ele.click()
        # ele = self.driver.find_element_by_xpath('//a[text()="添加成员"]')
        # print(ele.text)
        # ele.click()
        #print(colRight.find_element_by_xpath('.//a[@class="js_add_member"]').text())

    #跳转到通讯录页面
    def goto_contact(self):
        return ContactPage(self.driver)
    #添加部门
    # def goto_add_department(self):
    #     self.driver.find_element(self.driver,self.ele_add_button).click()
    #     self.driver.find_element(self.driver, self.ele_add_department).click()
    #     return AddDepartmentDialog(self.driver)

    # 跳转到导入通讯录页面
    def goto_import_contact(self):
        self.driver.find_element(By.CSS_SELECTOR, ".frame_nav_item_title").click()
        self.driver.implicitly_wait(3)
        #self.driver.find_element(By.CSS_SELECTOR,"index_service_cnt_item_title").click()
        return ImportContact(self.driver)


