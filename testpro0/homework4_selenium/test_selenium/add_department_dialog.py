from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait

from homework4_selenium.test_selenium.base_page import BasePage


from homework4_selenium.testcases.conftest import find_element
#from homework4_selenium.test_selenium.contact_page import ContactPage

#添加部门弹框
class AddDepartmentDialog(BasePage):
    add_department_dialog = (By.CSS_SELECTOR, '#__dialog__MNDialog__')
    ele_department_name = (
        By.CSS_SELECTOR, "div.qui_dialog_body.ww_dialog_body > div > form > div:nth-child(1) > input")
    ele_sumbit = (By.CSS_SELECTOR, "div.qui_dialog_foot.ww_dialog_foot > a.qui_btn.ww_btn.ww_btn_Blue")

    #添加部门
    def add_department(self,department):
        #显示等待，等待弹框加载
        def wait(x):
            find_element(self.driver, self.add_department_dialog)
        WebDriverWait(self.driver, 10).until(wait)

        find_element(self.driver, "qui_inputText ww_inputText").click()
        find_element(self.driver,self.ele_department_name).send_keys(department)
        find_element(self.driver, self.ele_sumbit).click()
        # 解决方案：https://www.cnblogs.com/91parson/p/11857664.html
        # 重复引用：https://blog.csdn.net/whatday/article/details/109333877
        # 显性等待,等待弹框加载
        # 检查是否成功添加部门,通过查找部门
        # def check_add_department(self, department):
        #     sleep(4)
        #     research = find_element(self.driver, self.ele_research)
        #     research.click()
        #     action = ActionChains(self.driver)
        #     action.send_keys(department).pause(1)
        #     action.perform()
        #     research_result = find_element(self.driver, self.ele_research_result)
        #     # print(research_result.text)
        #     return research_result.text   qui_inputText ww_inputText

        #
        # find_element(self.driver, self.ele_department_name).send_keys(department)
        # find_element(self.driver, self.ele_sumbit).click()
       #
       # return department



