from time import sleep

from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By

from homework4_selenium.test_selenium.add_department_dialog import AddDepartmentDialog
from homework4_selenium.test_selenium.add_member_page import AddMemberPage
from homework4_selenium.test_selenium.base_page import BasePage
from homework4_selenium.test_selenium.import_contact import ImportContact

from homework4_selenium.testcases.conftest import find_element, menu_selected, find_elements


class ContactPage(BasePage):
    contact_url = 'https://work.weixin.qq.com/wework_admin/frame#contacts'
    ele_add_member = (By.CSS_SELECTOR, "a.qui_btn.ww_btn.js_add_member:nth-child(2)")
    ele_add_department = (By.CSS_SELECTOR, '.js_add_sub_party')
    ele_add_button = (By.CSS_SELECTOR, 'member_colLeft_top_addBtn')
    ele_research = (By.ID, 'memberSearchInput')
    ele_research_result = (By.CSS_SELECTOR, '#search_party_list > li > a')

    # ele_add_file = (By.CSS_SELECTOR, 'ww_btnWithMenu js_btnWithMenu js_import_other_wrap')
    ele_add_file_to = (By.CSS_SELECTOR, ".ww_btn_PartDropdown_left")
    ele_add_file = (By.CSS_SELECTOR, ".js_import_member")
    #ele_add_file_to = (By.CSS_SELECTOR, 'qui_dropdownMenu_itemLink ww_dropdownMenu_itemLink js_import_member')
    ele_add_sure = (By.CSS_SELECTOR, 'submit_csv')
    ele_add_find = (By.CSS_SELECTOR, "reloadContact")

    # 初始化driver
    def __init__(self, driver):
        # self.driver = driver
        # 调用BasePage的__init__方法
        super(ContactPage, self).__init__(driver)
        # 菜单栏的通讯录不为选中状态，则跳转到通讯录页面
        if menu_selected(find_element(driver, (By.ID, 'menu_contacts'))) is not True:
            driver.get(self.contact_url)
        sleep(3)

    # 检查是否成功添加
    def check_add_memeber(self):
        sleep(3)
        mobile_nums = self.driver.find_elements(By.CSS_SELECTOR, ".member_colRight_memberTable_td:nth-child(5)")
        sleep(5)
        mobile_phone_list = []
        for i in mobile_nums:
            mobile_phone_list.append(i.text)
        sleep(5)
        # print(mobile_phone_list)
        return mobile_phone_list

    # 点击+号弹出添加部门弹框
    def goto_add_department(self):
        find_element(self.driver, self.ele_add_button).click()
        find_element(self.driver, self.ele_add_department).click()
        return AddDepartmentDialog(self.driver)

    # 检查是否成功添加部门,通过查找部门
    def check_add_department(self, department):
        sleep(4)

        research = find_element(self.driver, self.ele_research).click()

        action = ActionChains(self.driver)
        action.send_keys(department[0]).pause(1)
        action.perform()
        research_result = find_element(self.driver, self.ele_research_result)
        print(research_result.text)
        result_aa = []
        result_aa.append(research_result.text)
        return result_aa

    # 添加成员
    def goto_add_member(self):

        action = ActionChains(self.driver)
        action.click(find_element(self.driver, self.ele_add_member))
        action.perform()
        return AddMemberPage(self.driver)

    # 检查成员是否添加
    def get_list(self):
        sleep(3)
        ele_list = self.driver.find_elements(By.CSS_SELECTOR, ".member_colRight_memberTable_td:nth-child(2)")
        sleep(5)
        name_list = []

        for i in ele_list:
            name_list.append(i.text)

        return name_list

    # 跳转到导入通讯录页面
    def goto_import_contact(self):
        sleep(3)
        # self.driver.find_elements(By.CSS_SELECTOR, ".ww_btn_PartDropdown_left")[0].click()
        find_elements(self.driver, self.ele_add_file_to)[1].click()
        # WebElement
        sleep(3)
        find_elements(self.driver, self.ele_add_file)[1].click()
        self.driver.implicitly_wait(3)

        #find_element(self.driver, self.ele_add_sure).click()
        #find_element(self.driver, self.ele_add_find).click()
        return ImportContact(self.driver)
