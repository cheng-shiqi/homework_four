from time import sleep

import yaml
from selenium.webdriver.common.by import By

from homework4_selenium.test_selenium.base_page import BasePage

from homework4_selenium.testcases.conftest import find_element,find_elements


class ImportContact(BasePage):
    ele_upload_id = (By.ID, "js_upload_file_input")
    ele_submit_id = (By.ID, "submit_csv")
    ele_reload_id = (By.ID, "reloadContact")
    # 导入通讯录
    def import_contact(self, abspath):
        # sendkeys只支持绝对路径
        print(abspath)
        find_element(self.driver, self.ele_upload_id).send_keys(abspath)
        find_element(self.driver, self.ele_submit_id).click()
        #self.driver.find_element_by_id("js_upload_file_input").send_keys(abspath)
        #self.driver.find_element_by_id("submit_csv").click()
        print("111")
        sleep(10)
        find_element(self.driver, self.ele_reload_id).click()
        # 导入成功后点击前往查看
        #self.driver.find_element_by_id("reloadContact").click()
        #return ContactPage(self.driver)
# ele_import_contact_Button = "js_upload_file_input"
# ele_import_contact = (By.CSS_SELECTOR, '.qui_btn.ww_btn.ww_btn_Large.ww_btn_Blue.ww_fileImporter_submit')
# ele_import_contact_submit = "submit_csv"
# goto_find_result="reloadContact"
# # 文件导入import
# def file_import(self, datas):
#     find_element(self.driver, self.ele_import_contact_Button).click()
#     file_list = find_element(self.driver,datas)
#
#     find_element(self.driver,self.ele_import_contact_submit).click()
#
#     sleep(5)
#     find_element(self.driver,self.goto_find_result).click()
#     return file_list
# 跳转到导入通讯录页面
# def goto_import_contact(self):
#     find_element(self.driver, self.ele_add_file)
#     find_element(self.driver, self.ele_add_file_to).click()
#     self.driver.implicitly_wait(3)
#
#     find_element(self.driver, self.ele_add_sure).click()
#     return ImportContact(self.driver)
